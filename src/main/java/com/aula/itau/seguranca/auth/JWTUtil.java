package com.aula.itau.seguranca.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "dukqvy3t39834bef87w";

    private static final Long TEMPO_DE_VALIDADE = 86000l;

    //Metodo para gerar o token
    public String gerarToken(String email){
        Date dataDeVencimento = new Date(System.currentTimeMillis()+TEMPO_DE_VALIDADE);

        String token = Jwts.builder().setSubject(email)
                .setExpiration(dataDeVencimento)
                .signWith(SignatureAlgorithm.HS512, SEGREDO.getBytes()).compact();

        return token;
    }

    //Metodos para validação do token
    public String getEmail(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    public Claims getClaims(String token){
        return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();

        Date dataDeExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if(email != null && dataDeExpiracao != null && dataAtual.before(dataDeExpiracao)){
            return true;
        }else{
            return false;
        }

    }
}
